use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

const SCREEN_WIDTH: usize = 50; // px
const SCREEN_HEIGHT: usize = 6; // px
//const SCREEN_WIDTH: usize = 7; // px // Test inputs
//const SCREEN_HEIGHT: usize = 3; // px

const PRINT_STEPS: bool = false;

fn print_screen(scrn : &Vec<Vec<u8>>) {
    for y in 0..SCREEN_HEIGHT {
        for x in 0..SCREEN_WIDTH {
            match scrn[x][y] {
                0 => print!(" "),
                1 => print!("█"),
                _ => panic!("Unknown problem printing screen"),
            }
        }
        println!();
    }
}

fn day08(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut screen: Vec<Vec<u8>> = vec![vec![0; SCREEN_HEIGHT]; SCREEN_WIDTH];

    for line in input {

        let words: Vec<_> = line.split_whitespace().collect();

        match words[0] {
            "rect" => {
                let dims: Vec<_> = words[1].split("x").map(|x| x.parse::<usize>().unwrap()).collect();
                let (xmax,ymax) = (dims[0], dims[1]); // (0 x 1) = (width x height)
                for x in 0..xmax {
                    for y in 0..ymax {
                        screen[x][y] = 1;
                    }
                }
            },
            "rotate" => {
                let row_or_col = &words[1];
                let row_col_id: Vec<_> = words[2].split("=").collect();
                let row_col_id: String = row_col_id[1].to_string();
                let row_col_id = row_col_id.parse::<u8>().unwrap();
                let amt = &words[4].parse::<u8>().unwrap();
                match row_or_col.as_ref() {
                    "row" => {
                        let mut new: Vec<u8> = (0..SCREEN_WIDTH).map(|x| screen[x as usize][row_col_id as usize]).collect();
                        new.rotate_right(*amt as usize);
                        for x in 0..SCREEN_WIDTH {
                            screen[x as usize][row_col_id as usize] = new[x as usize];
                        }
                    },
                    "column" => {
                        let mut new: Vec<u8> = (0..SCREEN_HEIGHT).map(|y| screen[row_col_id as usize][y as usize]).collect();
                        new.rotate_right(*amt as usize);
                        for y in 0..SCREEN_HEIGHT {
                            screen[row_col_id as usize][y as usize] = new[y as usize];
                        }
                    },
                    _ => panic!("ERROR: Unknown rotation option."),
                }
            },
            _ => panic!("Unknown command"),
        };
        if PRINT_STEPS { print_screen(&screen); }
    }

    // Outputs
    let part1: u8 = screen.iter().flatten().sum();
    println!("Part 1: {}", part1); // 121

    println!("Part 2:"); // RURUCEOEIL
    print_screen(&screen);

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day08(&filename).unwrap();
}