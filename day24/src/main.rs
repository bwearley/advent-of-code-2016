use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;
use permutator::{Permutation};

extern crate permutator;

type HVACMap = Vec<Vec<MapPoint>>;

#[derive(Debug, Copy, Clone, PartialEq)]
enum MapType {
    Wall,
    Open,
    Target,
}

#[derive(Debug, Copy, Clone)]
struct MapPoint {
    x: usize,
    y: usize,
    d: usize,
    name: char,
    kind: MapType,
}
impl MapPoint {
    pub fn new_open(x: usize, y: usize) -> Self {
        Self {
            x: x,
            y: y,
            d: 0,
            name: '.',
            kind: MapType::Open,
        }
    }
}
impl PartialEq for MapPoint {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y && self.kind == other.kind
    }
}

struct TargetDistances {
    first: char,
    second: char,
    d: usize,
}

fn valid_neighbors(map: &HVACMap, pt: MapPoint) -> Vec<MapPoint> {
    let (x,y) = (pt.x, pt.y);
    let possible_neighbors = vec![(x-1,y), (x+1,y), (x,y-1), (x,y+1)];
    possible_neighbors
        .iter()
        .filter(|n| is_open(map, n.0, n.1))
        .map(|n| MapPoint::new_open(n.0, n.1))
        .collect()
}

fn is_open(map: &HVACMap, x: usize, y: usize) -> bool {
    //if x < 0 { return false; }
    //if y < 0 { return false; }
    if y >= map.len() { return false; }
    if x >= map[0].len() { return false; }
    map[y][x].kind == MapType::Open || map[y][x].kind == MapType::Target
}

fn point_distance(map: &HVACMap, x0: usize, y0: usize, x1: usize, y1: usize) -> usize {
    let mut q: VecDeque<MapPoint> = VecDeque::new();

    let ymax = map.len();
    let xmax = map[0].len();

    let mut distance: Vec<Vec<usize>> = vec![vec![std::usize::MAX; ymax as usize]; xmax as usize];

    // Insert first point
    q.push_back(MapPoint::new_open(x0, y0));

    while q.len() > 0 {
        let node = q.pop_front().unwrap();
        for mut n in valid_neighbors(map, node) {
            let new_dist = &node.d + 1;
            if new_dist < distance[n.x as usize][n.y as usize] {
                distance[n.x as usize][n.y as usize] = new_dist;
                n.d = new_dist;
                q.push_back(n);
            }
        }
    }
    distance[x1 as usize][y1 as usize]
}

fn day24(input: &str, debug: bool) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Build map
    let mut hvac_map: HVACMap = HVACMap::new();
    let mut targets: Vec<MapPoint> = Vec::new();
    let mut origin: MapPoint = MapPoint{x: 0, y: 0, d: 0, name: '0', kind: MapType::Target};
    for (y,line) in reader.lines().enumerate() {
        let mut map_row: Vec<MapPoint> = Vec::new();
        for (x,c) in line?.chars().enumerate() {
            match c {
                '#' => map_row.push(MapPoint{x: x, y: y, d: 0, name: c, kind: MapType::Wall}),
                '.' => map_row.push(MapPoint{x: x, y: y, d: 0, name: c, kind: MapType::Open}),
                '0'..='9' => {
                       map_row.push(MapPoint{x: x, y: y, d: 0, name: c, kind: MapType::Target});
                       targets.push(MapPoint{x: x, y: y, d: 0, name: c, kind: MapType::Target});
                },
                _ => panic!("Unknown character: {}",c),
            }
            if c == '0' { origin = MapPoint{x: x, y: y, d: 0, name: c, kind: MapType::Target} }
        }
        hvac_map.push(map_row);
    }
    let ymax = hvac_map.len();
    let xmax = hvac_map[0].len();

    // Draw Map
    if debug {
        println!("HVAC Map:");
        for y in 0..ymax {
            for x in 0..xmax {
                match hvac_map[y][x].kind {
                    MapType::Open   => print!("."),
                    MapType::Wall   => print!("#"),
                    MapType::Target => print!("{}", hvac_map[y][x].name),
                }
            }
            println!();
        }
    }

    // Calculate distances between targets
    let mut distances: Vec<TargetDistances> = Vec::new(); 
    for target in &targets {
        for other_target in &targets {
            if target == other_target { continue; }
            let new_dist = point_distance(&hvac_map, target.x, target.y, other_target.x, other_target.y);
            distances.push(TargetDistances{ first: target.name, second: other_target.name, d: new_dist});
        }
    }

    // Print calculated distances
    if debug {
        for dist in &distances {
            println!("Distance from {} to {}: {}", dist.first, dist.second, dist.d);
        }
    }

    let dist_from_to = |from: &MapPoint, to: &MapPoint, distances: &Vec<TargetDistances>| {
        distances.iter().find(|x| x.first == from.name && x.second == to.name).unwrap().d
    };

    // Parts 1 & 2
    let mut part1 = std::usize::MAX;
    let mut part2 = std::usize::MAX;
    for path in targets.permutation() {
        if path[0] != origin { continue; } // consider only paths that start at '0'
        let mut len_p1: usize = 0;
        let mut len_p2: usize = 0;
        for (ix,city) in path.iter().enumerate() {
            if ix == path.len() - 1 { 
                len_p2 += dist_from_to(&city, &origin, &distances); // go back to '0' if last city
            } else {
                len_p1 += dist_from_to(&city, &path[ix+1], &distances);
                len_p2 += dist_from_to(&city, &path[ix+1], &distances);
            }
        }
        if len_p1 < part1 { part1 = len_p1; }
        if len_p2 < part2 { part2 = len_p2; }
    }
    println!("Part 1: {}", part1); // 412
    println!("Part 2: {}", part2); // 664

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let debug = if args.len() >=3 { if &args[2] == "--debug" { true } else { false } } else { false };
    day24(&filename, debug).unwrap();
}