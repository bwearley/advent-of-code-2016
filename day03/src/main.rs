use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

#[derive(Debug, Copy, Clone)]
struct Triangle {
    a: i64,
    b: i64,
    c: i64,
}
impl Triangle {
    pub fn new(a: i64, b: i64, c: i64) -> Self {
        let mut v = vec![a, b, c]; v.sort();
        Self {
            a: v[0],
            b: v[1],
            c: v[2],
        }
    }
    pub fn is_valid(&self) -> bool {
        self.a + self.b > self.c
    }
}
impl From<&String> for Triangle {
    fn from(input: &String) -> Self {
        let mut parts: Vec<i64> = input
            .split_ascii_whitespace()
            .map(|x| x.parse::<i64>().unwrap())
            .collect();
        parts.sort();
        Self {
            a: parts[0],
            b: parts[1],
            c: parts[2],
        }
    }
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let lines: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let triangles: Vec<Triangle> = lines.iter().map(Triangle::from).collect();

    // Part 1 (993)
    println!("Part 1: {}", triangles.iter().filter(|t| t.is_valid()).count());

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    
    let mut triangles: Vec<Triangle> = Vec::new();

    for t in input.chunks(3) {
        let parts1: Vec<i64> = t[0].split_ascii_whitespace().map(|x| x.parse::<i64>().unwrap()).collect();
        let parts2: Vec<i64> = t[1].split_ascii_whitespace().map(|x| x.parse::<i64>().unwrap()).collect();
        let parts3: Vec<i64> = t[2].split_ascii_whitespace().map(|x| x.parse::<i64>().unwrap()).collect();
        triangles.push(Triangle::new(parts1[0], parts2[0], parts3[0]));
        triangles.push(Triangle::new(parts1[1], parts2[1], parts3[1]));
        triangles.push(Triangle::new(parts1[2], parts2[2], parts3[2]));
    }

    // Part 2 (1849)
    println!("Part 2: {}", triangles.iter().filter(|t| t.is_valid()).count());

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}