pub mod map {

    // enum MapDimension {
    //     Dim2D,
    //     Dim3D,
    // }

    #[derive(Debug, Copy, Clone)]
    pub struct Point {
        pub x: i64,
        pub y: i64,
        pub z: i64,
        pub value: i64,
    }
    impl Point {
        pub fn new() -> Point {
            Point {
                x: 0, y: 0, z: 0, value: 0
            }
        }
        pub fn from_xyz(x: i64, y: i64, z: i64) -> Point {
            Point {
                x: x,
                y: y,
                z: z,
                value: 0,
            }
        }
        pub fn from_xyz_value(x: i64, y: i64, z: i64, value: i64) -> Point {
            Point {
                x: x,
                y: y,
                z: z,
                value: value,
            }
        }
        pub fn from_xy(x: i64, y: i64) -> Point {
            Point {
                x: x,
                y: y,
                z: 0,
                value: 0,
            }
        }
        pub fn from_xy_value(x: i64, y: i64, value: i64) -> Point {
            Point {
                x: x,
                y: y,
                z: 0,
                value: value,
            }
        }
        pub fn set_value(&mut self, value: i64) {
            // if let mut v = self.value {
            //     v = value;
            // }
            self.value = value;
        }
        pub fn value(&self) -> i64 {
            self.value
        }
    }

    pub struct Map {
        map: Vec<Vec<Vec<Point>>>,
        x_min: i64, x_max: i64,
        y_min: i64, y_max: i64,
        z_min: i64, z_max: i64,
        //dimension: MapDimension,
    }
    impl Map {
        fn xoffs(&self, xin: i64) -> usize { (xin - self.x_min) as usize }
        fn yoffs(&self, yin: i64) -> usize { (yin - self.y_min) as usize }
        fn zoffs(&self, zin: i64) -> usize { (zin - self.z_min) as usize }
        
        pub fn new2d(xmin: i64, xmax: i64, ymin: i64, ymax: i64) -> Map {
            let zmin: i64 = 0;
            let zmax: i64 = 0;

            //let dim = MapDimension::Dim2D;

            let xsize: usize = (xmax - xmin + 1) as usize;
            let ysize: usize = (ymax - ymin + 1) as usize;
            let zsize: usize = (zmax - zmin + 1) as usize;
            Map {
                map: vec![vec![vec![Point::new(); zsize]; ysize]; xsize],
                x_min: xmin,
                x_max: xmax,
                y_min: ymin,
                y_max: ymax,
                z_min: zmin,
                z_max: zmax,
                //dimension: dim,
            }
        }

        pub fn new3d(xmin: i64, xmax: i64, ymin: i64, ymax: i64, zmin:i64, zmax: i64) -> Map {

            //let dim = MapDimension::Dim3D;

            let xsize: usize = (xmax - xmin + 1) as usize;
            let ysize: usize = (ymax - ymin + 1) as usize;
            let zsize: usize = (zmax - zmin + 1) as usize;
            Map {
                map: vec![vec![vec![Point::new(); zsize]; ysize]; xsize],
                x_min: xmin,
                x_max: xmax,
                y_min: ymin,
                y_max: ymax,
                z_min: zmin,
                z_max: zmax,
                //dimension: dim,
            }
        }
        pub fn init(&mut self) {
            for x in self.x_min..=self.x_max {
                for y in self.y_min..=self.y_max {
                    for z in self.z_min..=self.z_max {
                        let (newx,newy,newz) = (self.xoffs(x), self.yoffs(y), self.zoffs(z));
                        self.map[newx][newy][newz] =
                            Point::from_xyz_value(x, y, z, 0);
                    }
                }
            }
        }
        fn point_exists(&self, x: i64, y: i64, z: i64) -> bool {
            is_between(x, self.x_min, self.x_max) && is_between(y, self.y_min, self.y_max) && is_between(z, self.z_min, self.z_max)
        }
        pub fn point2d(&self, x: i64, y: i64) -> &Point {
            if !self.point_exists(x, y, 0) {
                panic!("ERROR: Tried to request invalid point ({},{},{})", x, y, 0);
            }
            let (newx,newy,newz) = (self.xoffs(x), self.yoffs(y), self.zoffs(0));
            return &self.map[newx][newy][newz];
        }
        pub fn point3d(&self, x: i64, y: i64, z: i64) -> &Point {
            if !self.point_exists(x, y, z) {
                panic!("ERROR: Tried to request invalid point ({},{},{})", x, y, z);
            }
            let (newx,newy,newz) = (self.xoffs(x), self.yoffs(y), self.zoffs(z));
            return &self.map[newx][newy][newz];
        }
        pub fn get_adjacent(&self, pt: &Point) -> Vec<&Point> {
            let mut neighbors: Vec<&Point> = Vec::new();
          //match self.dimension {
          //    MapDimension::Dim2D => {
          //        // NSEW Neighbors
          //        if self.point_exists(pt.x+0,pt.y-1,pt.z) { neighbors.push(self.point3d(pt.x+0,pt.y-1,pt.z)); }
          //        if self.point_exists(pt.x-1,pt.y+0,pt.z) { neighbors.push(self.point3d(pt.x-1,pt.y+0,pt.z)); }
          //        if self.point_exists(pt.x+1,pt.y+0,pt.z) { neighbors.push(self.point3d(pt.x+1,pt.y+0,pt.z)); }
          //        if self.point_exists(pt.x+0,pt.y+1,pt.z) { neighbors.push(self.point3d(pt.x+0,pt.y+1,pt.z)); }
          //        // Diagonal Neighbors
          //        if self.point_exists(pt.x-1,pt.y-1,pt.z) { neighbors.push(self.point3d(pt.x-1,pt.y-1,pt.z)); }
          //        if self.point_exists(pt.x-1,pt.y+1,pt.z) { neighbors.push(self.point3d(pt.x-1,pt.y+1,pt.z)); }
          //        if self.point_exists(pt.x+1,pt.y-1,pt.z) { neighbors.push(self.point3d(pt.x+1,pt.y-1,pt.z)); }
          //        if self.point_exists(pt.x+1,pt.y+1,pt.z) { neighbors.push(self.point3d(pt.x+1,pt.y+1,pt.z)); }
          //    },
          //    MapDimension::Dim3D => {
                    // NSEWUD Neighbors
                    if self.point_exists(pt.x+0,pt.y-1,pt.z+0) { neighbors.push(self.point3d(pt.x+0,pt.y-1,pt.z+0)); }
                    if self.point_exists(pt.x-1,pt.y+0,pt.z+0) { neighbors.push(self.point3d(pt.x-1,pt.y+0,pt.z+0)); }
                    if self.point_exists(pt.x+1,pt.y+0,pt.z+0) { neighbors.push(self.point3d(pt.x+1,pt.y+0,pt.z+0)); }
                    if self.point_exists(pt.x+0,pt.y+1,pt.z+0) { neighbors.push(self.point3d(pt.x+0,pt.y+1,pt.z+0)); }
                    if self.point_exists(pt.x+0,pt.y+0,pt.z-1) { neighbors.push(self.point3d(pt.x+0,pt.y+0,pt.z-1)); }
                    if self.point_exists(pt.x+0,pt.y+0,pt.z+1) { neighbors.push(self.point3d(pt.x+0,pt.y+0,pt.z+1)); }
                    // Other/Diagonal Neighbors
                    if self.point_exists(pt.x-1,pt.y-1,pt.z+0) { neighbors.push(self.point3d(pt.x-1,pt.y-1,pt.z+0)); }
                    if self.point_exists(pt.x-1,pt.y+1,pt.z+0) { neighbors.push(self.point3d(pt.x-1,pt.y+1,pt.z+0)); }
                    if self.point_exists(pt.x+1,pt.y-1,pt.z+0) { neighbors.push(self.point3d(pt.x+1,pt.y-1,pt.z+0)); }
                    if self.point_exists(pt.x+1,pt.y+1,pt.z+0) { neighbors.push(self.point3d(pt.x+1,pt.y+1,pt.z+0)); }

                    if self.point_exists(pt.x-1,pt.y-1,pt.z-1) { neighbors.push(self.point3d(pt.x-1,pt.y-1,pt.z-1)); }
                    if self.point_exists(pt.x-1,pt.y+1,pt.z-1) { neighbors.push(self.point3d(pt.x-1,pt.y+1,pt.z-1)); }
                    if self.point_exists(pt.x+1,pt.y-1,pt.z-1) { neighbors.push(self.point3d(pt.x+1,pt.y-1,pt.z-1)); }
                    if self.point_exists(pt.x+1,pt.y+1,pt.z-1) { neighbors.push(self.point3d(pt.x+1,pt.y+1,pt.z-1)); }

                    if self.point_exists(pt.x-1,pt.y-1,pt.z+1) { neighbors.push(self.point3d(pt.x-1,pt.y-1,pt.z+1)); }
                    if self.point_exists(pt.x-1,pt.y+1,pt.z+1) { neighbors.push(self.point3d(pt.x-1,pt.y+1,pt.z+1)); }
                    if self.point_exists(pt.x+1,pt.y-1,pt.z+1) { neighbors.push(self.point3d(pt.x+1,pt.y-1,pt.z+1)); }
                    if self.point_exists(pt.x+1,pt.y+1,pt.z+1) { neighbors.push(self.point3d(pt.x+1,pt.y+1,pt.z+1)); }

                    if self.point_exists(pt.x+0,pt.y-1,pt.z-1) { neighbors.push(self.point3d(pt.x+0,pt.y-1,pt.z-1)); }
                    if self.point_exists(pt.x-1,pt.y+0,pt.z-1) { neighbors.push(self.point3d(pt.x-1,pt.y+0,pt.z-1)); }
                    if self.point_exists(pt.x+1,pt.y+0,pt.z-1) { neighbors.push(self.point3d(pt.x+1,pt.y+0,pt.z-1)); }
                    if self.point_exists(pt.x+0,pt.y+1,pt.z-1) { neighbors.push(self.point3d(pt.x+0,pt.y+1,pt.z-1)); }

                    if self.point_exists(pt.x+0,pt.y-1,pt.z+1) { neighbors.push(self.point3d(pt.x+0,pt.y-1,pt.z+1)); }
                    if self.point_exists(pt.x-1,pt.y+0,pt.z+1) { neighbors.push(self.point3d(pt.x-1,pt.y+0,pt.z+1)); }
                    if self.point_exists(pt.x+1,pt.y+0,pt.z+1) { neighbors.push(self.point3d(pt.x+1,pt.y+0,pt.z+1)); }
                    if self.point_exists(pt.x+0,pt.y+1,pt.z+1) { neighbors.push(self.point3d(pt.x+0,pt.y+1,pt.z+1)); }

          //    },
          //    //_ => panic!("Unknown map dimension in get_adjacent."),
          //}
            neighbors
        }
    }
    fn is_between(val: i64, min: i64, max: i64) -> bool {
        val >= min && val <= max
    }
}


// #[cfg(test)]
// mod tests {
//     #[test]
//     fn it_works() {
//         assert_eq!(2 + 2, 4);
//     }
// }
