use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;

const XMAX: i64 = 1000;
const YMAX: i64 = 1000;

const PRINT_MAP: bool = false;

fn poly(x: i64, y: i64) -> i64 {
    x*x + 3*x + 2*x*y + y + y*y
}

fn map_type(x: i64, y: i64, fav: i64) -> MapType {
    let num = (poly(x,y) + fav) as usize;
    match num.count_ones() % 2 {
        0 => MapType::Open,
        _ => MapType::Wall,
    }
}

enum MapType {
    Wall,
    Open,
}

#[derive(Debug, Copy, Clone)]
struct MapPoint {
    x: i64,
    y: i64,
    d: i64,
}

fn valid_neighbors(pt: MapPoint) -> Vec<MapPoint> {
    let (x,y) = (pt.x, pt.y);
    let mut neighbors: Vec<MapPoint> = Vec::new();
    if x-1 >= 0   { neighbors.push(MapPoint{x:x-1,y:y  ,d:0})}
    if y-1 >= 0   { neighbors.push(MapPoint{x:x  ,y:y-1,d:0})}
    if x+1 < XMAX { neighbors.push(MapPoint{x:x+1,y:y  ,d:0})}
    if y+1 < YMAX { neighbors.push(MapPoint{x:x  ,y:y+1,d:0})}
    if x+1 >= XMAX || y+1 >= YMAX { panic!("Failed to make a big enough map."); }
    neighbors
}

fn point_distance(x0: i64, y0: i64, x1: i64, y1: i64) -> i64 {
    let mut q: VecDeque<MapPoint> = VecDeque::new();

    let mut visited: Vec<Vec<bool>> = vec![vec![ false; YMAX as usize]; XMAX as usize];
    let mut distance: Vec<Vec<i64>> = vec![vec![std::i64::MAX; YMAX as usize]; XMAX as usize];

    // Insert first point
    q.push_back(MapPoint{x:x0,y:y0,d:0});

    while q.len() > 0 {
        let node = q.pop_front().unwrap();
        for mut n in valid_neighbors(node) {
            if visited[n.x as usize][n.y as usize] { continue; }
            visited[n.x as usize][n.y as usize] = true;
            match map_type(n.x, n.y, 1362) {
                MapType::Wall => continue,
                MapType::Open => {},
            }
            let new_dist = &node.d + 1;
            if new_dist < distance[n.x as usize][n.y as usize] {
                
                distance[n.x as usize][n.y as usize] = new_dist;
                n.d = new_dist;
                q.push_back(n);
            }
        }
    }
    distance[x1 as usize][y1 as usize]
}

fn day13(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat().parse::<i64>().unwrap();

    // Draw Map
    if PRINT_MAP {
        for y in 0..=50 {
            for x in 0..=50 {
                if x == 1 && y == 1 { print!("X"); continue; }
                match map_type(x,y,input) {
                    MapType::Open => print!("."),
                    MapType::Wall => print!("#"),
                }
            }
            println!();
        }
    }

    // Part 1: 82
    println!("Part 1: {}", point_distance(1, 1, 31, 39));

    // Part 2: 138
    let mut part2 = 0;
    for y in 0..=30 {
        for x in 0..=30 {
            if point_distance(1, 1, x, y) <= 50 { part2 += 1; }
        }
    }
    println!("Part 2: {}", part2); // 138

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day13(&filename).unwrap();
}