use std::env;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

#[macro_use]
extern crate text_io;

struct Room {
    name: String,
    checksum: String,
    sector: i64,
}
impl Room {
    pub fn from_string(input: &str) -> Self {
        let name_sector: String;
        let checksum: String;
        let mut name = String::new();
        let mut sector_string = String::new();
        scan!(input.bytes() => "{}[{}]", name_sector, checksum);
        let mut parts = name_sector.split("-").peekable();
        while let Some(part) = parts.next() {
            if parts.peek().is_some() {
                // parts before last part are room name
                name += &part;
            } else {
                // last part is sector ID
                sector_string = part.to_string();
            }
        }
        Self {
            name: name,
            checksum: checksum,
            sector: sector_string.parse::<i64>().unwrap(),
        }
    }
    pub fn is_valid(&self) -> bool {
        checksum(&self.name) == self.checksum
    }
    pub fn real_name(&self) -> String {
        self.name.chars().map(|c| {
            match c {
                'a'..='z' => (((c as u32 - b'a' as u32 + self.sector as u32) % 26) as u8 + b'a') as char,
                _ => panic!("Unknown error"),
            }
        }).collect()
    }
}

fn checksum(string: &String) -> String {
    let mut freq: HashMap<char, u32> = HashMap::new();
    for c in string.chars() {
        *freq.entry(c).or_insert(0) += 1;
    }
    let mut count_vec: Vec<(&char, &u32)> = freq.iter().collect();
    count_vec.sort_by(|a, b| {
        match b.1.cmp(a.1) { // sort by frequency
            Ordering::Equal => b.0.cmp(a.0).reverse(), // sort alphabetically
            other => other,
        }
    });
    count_vec[0..5]
        .into_iter()
        .map(|x| x.0.to_string())
        .collect::<Vec<String>>()
        .concat()
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let rooms: Vec<Room> = input.iter().map(|r| Room::from_string(r)).collect();

    // Part 1
    let part1: i64 = rooms.iter().filter(|x| x.is_valid()).map(|x| x.sector).sum();
    println!("Part 1: {}", part1); // 409147

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let rooms: Vec<Room> = input.iter().map(|r| Room::from_string(r)).collect();

    // Part 2
    let part2 = rooms.iter().find(|x| x.real_name() == "northpoleobjectstorage").unwrap().sector;
    println!("Part 2: {}", part2); // 991

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}