use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate regex;
use regex::Regex;

fn decompress_v1(input: &str) -> String {
    let mut decompressed = String::new();
    let re = Regex::new(r"^\(([0-9]+)x([0-9]+)\)").unwrap();
    let mut ix: usize = 0;
    while ix < input.len() {
        let caps = re.captures(&input[ix..]);
        if let Some(cap) = caps {
            let length = &cap[1].parse::<usize>().unwrap();
            let repeats = &cap[2].parse::<usize>().unwrap();
            let skip_len = &cap[1].len() + &cap[2].len() + 3; // skip "(__x__)"
            ix += skip_len;
            for _ in 0..*repeats {
                decompressed.push_str(&input[ix..ix+*length]);
            }
            ix += *length;
        } else {
            decompressed.push_str(&input[ix..ix+1]);
            ix +=1;
        }
    }
    decompressed
}

fn decompress_v2_len(input: &str) -> i64 {
    let mut len: i64 = 0;
    let re = Regex::new(r"^\(([0-9]+)x([0-9]+)\)").unwrap();
    let mut ix: usize = 0;
    while ix < input.len() {
        let caps = re.captures(&input[ix..]);
        if let Some(cap) = caps {
            let length = &cap[1].parse::<usize>().unwrap();
            let repeats = &cap[2].parse::<usize>().unwrap();
            let skip_len = &cap[1].len() + &cap[2].len() + 3; // skip "(__x__)"
            ix += skip_len;
            let repeat_string = &input[ix..ix+*length];
            let len2 = decompress_v2_len(repeat_string);
            for _ in 0..*repeats {
                len += len2;
            }
            ix += *length;
        } else {
            len += 1;
            ix +=1;
        }
    }
    len
}

fn decompress_v2(input: &str) -> String {
    let mut decompressed = String::new();
    let re = Regex::new(r"^\(([0-9]+)x([0-9]+)\)").unwrap();
    let mut ix: usize = 0;
    while ix < input.len() {
        let caps = re.captures(&input[ix..]);
        if let Some(cap) = caps {
            let length = &cap[1].parse::<usize>().unwrap();
            let repeats = &cap[2].parse::<usize>().unwrap();
            let skip_len = &cap[1].len() + &cap[2].len() + 3; // skip "(__x__)"
            ix += skip_len;
            let repeat_string = &input[ix..ix+*length];
            let repeat_string2 = decompress_v2(repeat_string);
            for _ in 0..*repeats {
                decompressed.push_str(&repeat_string2);
            }
            ix += *length;
        } else {
            decompressed.push_str(&input[ix..ix+1]);
            ix +=1;
        }
    }
    decompressed
}
fn day09(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Outputs
    println!("Part 1: {}", decompress_v1(&input).len()); // 183269
    println!("Part 2: {}", decompress_v2_len(&input)); // 11317278863

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day09(&filename).unwrap();
}