use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::cell::RefCell;

#[derive(Debug, Copy, Clone)]
enum InstructionTarget {
    Register,
    Value,
}

#[derive(Debug, Copy, Clone)]
enum Instr {
    Inc,
    Dec,
    Tgl,
    Jnz,
    Cpy,
    Out,
}

#[derive(Debug, Copy, Clone)]
struct Instruction {
    name: Instr,
    x: i64,
    y: i64,
    xtarget: InstructionTarget,
    ytarget: InstructionTarget,
}
impl Instruction {
    pub fn from_string(input: &str) -> Self {
        let mut parts = input.split_whitespace();
        let name = match parts.next().unwrap() {
            "inc" => Instr::Inc,
            "dec" => Instr::Dec,
            "tgl" => Instr::Tgl,
            "jnz" => Instr::Jnz,
            "cpy" => Instr::Cpy,
            "out" => Instr::Out,
            other => panic!("Unknown instruction: {}", other),
        };
        let (xtarget,x) = match parts.next() {
            Some("a")   => (InstructionTarget::Register, 0),
            Some("b")   => (InstructionTarget::Register, 1),
            Some("c")   => (InstructionTarget::Register, 2),
            Some("d")   => (InstructionTarget::Register, 3),
            Some(value) => (InstructionTarget::Value, value.parse::<i64>().unwrap()),
            None        => panic!("Bad instruction"),
        };
        let (ytarget,y) = match parts.next() {
            Some("a")   => (InstructionTarget::Register, 0),
            Some("b")   => (InstructionTarget::Register, 1),
            Some("c")   => (InstructionTarget::Register, 2),
            Some("d")   => (InstructionTarget::Register, 3),
            Some(value) => (InstructionTarget::Value, value.parse::<i64>().unwrap()),
            None        => (InstructionTarget::Value, 0),
        };
        Self { name, x, y, xtarget, ytarget }
    }
    pub fn tgl(&mut self) {
        self.name = match self.name {
            Instr::Inc => Instr::Dec,
            Instr::Dec => Instr::Inc,
            Instr::Tgl => Instr::Inc,
            Instr::Jnz => Instr::Cpy,
            Instr::Cpy => Instr::Jnz,
            Instr::Out => Instr::Inc,
        };
    }
}

fn day25(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let instrs: Vec<Instruction> = reader
        .lines()
        .map(|x| Instruction::from_string(&x.unwrap()))
        .collect();

    let instrs_saved = instrs.clone();
    let instrs: RefCell<Vec<Instruction>> = RefCell::new(instrs);

    const A: usize = 0;
    const B: usize = 1;
    const C: usize = 2;
    const D: usize = 3;

    let reg: RefCell<Vec<i64>> = RefCell::new(vec![0; 4]);

    let current: RefCell<i64> = RefCell::new(0);

    let signal: RefCell<Vec<i64>> = RefCell::new(Vec::with_capacity(100));

    let get_val = |a: i64, target: InstructionTarget| {
        match target {
            InstructionTarget::Register => return reg.borrow()[a as usize],
            InstructionTarget::Value    => return a,
        }
    };

    let cpy = |x: i64, y: i64, xt: InstructionTarget, yt: InstructionTarget| {
        let valx = get_val(x, xt);
        match yt {
            InstructionTarget::Register => { reg.borrow_mut()[y as usize] = valx; },
            InstructionTarget::Value    => {},
        }
    };

    let jnz = |x: i64, y: i64, xt: InstructionTarget, yt: InstructionTarget| {
        let valx = get_val(x, xt);
        let valy = get_val(y, yt);
        if valx != 0 { *current.borrow_mut() += valy - 1; }
    };

    let inc = |x: i64, xt: InstructionTarget| {
        match xt {
            InstructionTarget::Register => { reg.borrow_mut()[x as usize] += 1; },
            InstructionTarget::Value    => {},
        }
    };

    let dec = |x: i64, xt: InstructionTarget| {
        match xt {
            InstructionTarget::Register => { reg.borrow_mut()[x as usize] -= 1; },
            InstructionTarget::Value    => {},
        }
    };

    let tgl = |x: i64, xt: InstructionTarget| {
        let valx = get_val(x, xt);
        let target_ins = *current.borrow() + valx;
        if target_ins >= instrs.borrow().len() as i64 || target_ins < 0 { return; }
        let target_ins = target_ins as usize;
        instrs.borrow_mut()[target_ins].tgl();
    };

    let out = |x: i64, xt: InstructionTarget| {
        let valx = get_val(x, xt);
        signal.borrow_mut().push(valx);
    };

    // Part 1
    let mut part1 = 0;
    'part1_lp: loop {
        part1 += 1;
        for (ix,_) in instrs_saved.iter().enumerate() { // Reset instructions
            instrs.borrow_mut()[ix] = instrs_saved[ix];
        }
        signal.borrow_mut().clear();
        *current.borrow_mut() = 0;
        reg.borrow_mut()[A] = part1;
        reg.borrow_mut()[B] = 0;
        reg.borrow_mut()[C] = 0;
        reg.borrow_mut()[D] = 0;
        loop {

            let (ins_name,ins_x,ins_y,ins_xtarget,ins_ytarget) = 
                (instrs.borrow()[*current.borrow() as usize].name.clone(),
                instrs.borrow()[*current.borrow() as usize].x,
                instrs.borrow()[*current.borrow() as usize].y,
                instrs.borrow()[*current.borrow() as usize].xtarget.clone(),
                instrs.borrow()[*current.borrow() as usize].ytarget.clone());

            match ins_name {
                Instr::Inc => inc(ins_x, ins_xtarget),
                Instr::Dec => dec(ins_x, ins_xtarget),
                Instr::Jnz => jnz(ins_x, ins_y, ins_xtarget, ins_ytarget),
                Instr::Cpy => cpy(ins_x, ins_y, ins_xtarget, ins_ytarget),
                Instr::Tgl => tgl(ins_x, ins_xtarget),
                Instr::Out => out(ins_x, ins_xtarget),
            }
            *current.borrow_mut() += 1;
            if *current.borrow() >= instrs.borrow().len() as i64 { break; }
            if signal.borrow().len() == 100 {
                for (sig_ix, freq) in signal.borrow().iter().enumerate() {
                    if sig_ix % 2 == 0 && freq == &0 { continue; }
                    if sig_ix % 2 != 0 && freq == &1 { continue; }
                    continue 'part1_lp;
                }
                break 'part1_lp;
            }
        }
    }

    // Outputs
    println!("Part 1: {}", part1); // 175

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day25(&filename).unwrap();
}