use std::env;
use std::collections::HashMap;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn day06(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Generate hash map of character frequency in each column
    let mut char_freq: Vec<HashMap<char, u32>> = vec![HashMap::new(); 8];
    for line in reader.lines() {
        for (i,c) in line?.chars().enumerate() {
            *char_freq[i].entry(c).or_insert(0) += 1;
        }
    }

    // Calculate parts 1 & 2
    let mut part1 = String::new();
    let mut part2 = String::new();
    for pos in char_freq {
        let mut x: Vec<(&char, &u32)> = pos.iter().collect();
        x.sort_by(|a, b| b.1.cmp(a.1)); // sort by most frequent
        part1.push_str(&x[0].0.to_string());
        x.sort_by(|a, b| b.1.cmp(a.1).reverse()); // sort by least frequent
        part2.push_str(&x[0].0.to_string());
    }

    // Part 1
    println!("Part 1: {}", part1); // umejzgdw
    println!("Part 2: {}", part2); // aovueakv

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day06(&filename).unwrap();
}