use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

#[macro_use]
extern crate text_io;

#[derive(Debug, Copy, Clone)]
struct StorageNode {
    x: isize,
    y: isize,
    size: isize,
    used: isize,
    avail: isize,
}
impl From<&String> for StorageNode {
    fn from(input: &String) -> Self {
        // Filesystem              Size  Used  Avail  Use%
        // /dev/grid/node-x0-y7     86T   65T    21T   75%
        let x: isize;
        let y: isize;
        let size: isize;
        let used: isize;
        let avail: isize;
        let _pct: isize;
        let mut parts = input.split_whitespace().collect::<Vec<&str>>();
        scan!(parts[0].bytes() => "/dev/grid/node-x{}-y{}", x, y);
        scan!(parts[1].bytes() => "{}T", size);
        scan!(parts[2].bytes() => "{}T", used);
        scan!(parts[3].bytes() => "{}T", avail);
        Self {
            x: x,
            y: y,
            size: size,
            used: used,
            avail: avail,
        }
    }
}
impl StorageNode {
    pub fn new() -> Self {
        Self {
            x: 0,
            y: 0,
            size: 0,
            used: 0,
            avail: 0,
        }
    }
}

fn day22(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let nodes_raw: Vec<StorageNode> = input[2..]
        .iter()
        .map(StorageNode::from)
        .collect();

    let mut part1 = 0;
    let mut walls: Vec<(usize,usize)> = Vec::new();
    for n in &nodes_raw {
        if n.used == 0 { continue; }
        let viable = &nodes_raw
            .iter()
            //.filter(|other| !(other.x == n.x && other.y == n.y) && n.used <= other.avail)
            .filter(|other| (other.x,other.y) != (n.x,n.y) && n.used <= other.avail)
            .count();
        if viable == &0 { walls.push( (n.x as usize, n.y as usize) ); }
        part1 += viable;
    }
    println!("Part 1: {}", part1); // 1045

    let xmax = nodes_raw.iter().max_by_key(|n| n.x).unwrap().x as usize;
    let ymax = nodes_raw.iter().max_by_key(|n| n.y).unwrap().y as usize;

    // Rebuild as 2D vectors
    let mut nodes: Vec<Vec<StorageNode>> = vec![vec![StorageNode::new(); ymax+1]; xmax+1];
    for y in 0..=ymax {
        for x in 0..=xmax {
            nodes[x][y] = *nodes_raw
                .iter()
                .filter(|n| n.x == x as isize && n.y == y as isize)
                .next()
                .unwrap();
        }
    }

    // Print map
    println!("Initial state:");
    print!(" x=");
    for x in 0..=xmax {
        print!(" {:2}",x);
    }
    println!("");
    for y in 0..=ymax {
        print!(" {:2} ",y);
        for x in 0..=xmax {
            match nodes[x][y].used {
                0 => print!(" _ "),
                _ => {
                    if x == 0 && y == 0 { print!(" ! "); continue; }
                    if x == xmax && y == 0 { print!(" G "); continue; }
                    if &walls.iter().filter(|(x0,y0)| x0 == &x && y0 == &y).count() > &0 {
                        print!(" # ");
                    } else {
                        print!(" . ");
                    }
                },
            }
        }
        println!("");
    }

    // Part 2
    // (35,27) -> (1,17) -> (0,34) -> 5*(34)
    // total_moves = wall_width + y0 + xmax + (xmax-1) * 5
    // Support vertical or horizontal walls
    let empty = nodes_raw
        .iter()
        .filter(|x| x.used == 0)
        .map(|n| (n.x, n.y))
        .next()
        .unwrap();

    let wall_width = std::cmp::max(
            walls.iter().max_by_key(|x| x.0).unwrap().0 as isize - walls.iter().min_by_key(|x| x.0).unwrap().0 as isize,
            walls.iter().max_by_key(|x| x.1).unwrap().1 as isize - walls.iter().min_by_key(|x| x.1).unwrap().1 as isize) as isize;

    let part2 = wall_width - (xmax as isize - empty.0) + empty.1 + (xmax as isize) + (xmax as isize - 1) * 5;
    println!("Part 2: {}", part2); // 265

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day22(&filename).unwrap();
}