use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

struct Elf {
    left: i64,
    right: i64,
    gifts: i64,
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.parse::<i64>().unwrap();

    // Initialize
    let mut elves: Vec<Elf> = Vec::new();
    for i in 0..input {
        elves.push(Elf{ left: i-1, right: i+1, gifts: 1});
    }
    elves[0].left = input-1;
    elves[(input-1) as usize].right = 0;

    // Do game
    let mut current: usize = 0; // elf 1
    let mut remaining = input;
    loop {
        let current_right = elves[current].right as usize;
        elves[current].gifts += elves[current_right].gifts;
        elves[current_right].gifts = 0;
        remaining -= 1;
        elves[current].right = elves[current_right].right;
        if remaining == 0 { break; }
        current = elves[current].right as usize;
    }

    println!("Part 1: {}", current+1); // 1842613

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.parse::<i64>().unwrap();

    // Initialize
    let mut elves: Vec<Elf> = Vec::new();
    for i in 0..input {
        elves.push(Elf{ left: i-1, right: i+1, gifts: 1});
    }
    elves[0].left = input-1;
    elves[(input-1) as usize].right = 0;

    // Do game
    let mut current: usize = 0; // elf 1
    let mut opposite: usize = (input / 2) as usize;
    let mut remaining = input;
    loop {
        // Take gifts
        elves[current].gifts += elves[opposite].gifts;
        elves[opposite].gifts = 0;

        // Remove opposite
        let elves_opposite_left = elves[opposite].left as usize;
        let elves_opposite_right = elves[opposite].right as usize;
        elves[elves_opposite_left].right = elves_opposite_right as i64;
        elves[elves_opposite_right].left = elves_opposite_left as i64;
        remaining -= 1;
        if remaining == 0 { break; }

        // Advance
        current = elves[current].right as usize;
        opposite = elves[opposite].right as usize;
        if remaining % 2 == 0 { opposite = elves[opposite].right as usize; }
    }

    println!("Part 2: {}", current+1); // 1424135

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}