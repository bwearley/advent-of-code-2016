use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use crypto::md5::Md5;
use crypto::digest::Digest;

fn get_triplet(input: &str) -> Option<char> {
    for chs in input.chars().collect::<Vec<_>>().windows(3) {
        let (a,b,c) = (chs[0], chs[1], chs[2]);
        if a == b && b == c { return Some(a); }
    }
    None
}

fn has_quint_of_char(input: &str, ch: char) -> bool {
    for chs in input.chars().collect::<Vec<_>>().windows(5) {
        let (a,b,c,d,e) = (chs[0], chs[1], chs[2], chs[3], chs[4]);
        if a == b && b == c && c == d && d == e && a == ch { return true; }
    }
    false
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: String = input.concat();

    // Pre-generate passwords
    let mut hashes: Vec<String> = Vec::new();
    const NUM_PRE_HASH: usize = 30_000;
    hashes.reserve(NUM_PRE_HASH);
    let mut digest = Md5::new();
    for index in 0..NUM_PRE_HASH {
        let hash = format!("{}{}", &input, &index);
        digest.reset();
        digest.input_str(&hash);
        hashes.push(digest.result_str());
    }

    // Generate password
    let mut keys: Vec<String> = Vec::new();
    let mut index: usize = 0;
    while keys.len() < 64 {
        if let Some(triplet) = get_triplet(&hashes[index]) {
            for ix2 in (index+1)..=(index+1000) {
                if has_quint_of_char(&hashes[ix2], triplet) {
                    keys.push(String::from(&hashes[index]));
                    break;
                }
            }
        }
        index += 1;
    }
    println!("Part 1: {}", index-1); // 25427

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: String = input.concat();

    // Pre-generate passwords
    let mut hashes: Vec<String> = Vec::new();
    const NUM_PRE_HASH: usize = 30_000;
    hashes.reserve(NUM_PRE_HASH);
    let mut digest = Md5::new();
    for index in 0..NUM_PRE_HASH {
        let hash = format!("{}{}", &input, &index);
        digest.reset();
        digest.input_str(&hash);
        for _ in 0..2016 {
            let digest_in = &digest.result_str();
            digest.reset();
            digest.input_str(digest_in);
        }
        hashes.push(digest.result_str());
    }

    // Generate password
    let mut keys: Vec<String> = Vec::new();
    let mut index: usize = 0;
    while keys.len() < 64 {
        if let Some(triplet) = get_triplet(&hashes[index]) {
            for ix2 in (index+1)..=(index+1000) {
                if has_quint_of_char(&hashes[ix2], triplet) {
                    keys.push(String::from(&hashes[index]));
                    break;
                }
            }
        }
        index += 1;
    }
    println!("Part 2: {}", index-1); // 22045

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}