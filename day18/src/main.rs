use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn is_trap(ch: char) -> bool {
    match ch {
        '^' => true,
        '|' | '.' => false,
        other => panic!("Unknown character: {}", other),
    }
}

fn new_tile(left: char, center: char, right: char) -> char {
    if  is_trap(left) &&  is_trap(center) && !is_trap(right) { return '^'; } // rule 1
    if !is_trap(left) &&  is_trap(center) &&  is_trap(right) { return '^'; } // rule 2
    if  is_trap(left) && !is_trap(center) && !is_trap(right) { return '^'; } // rule 3
    if !is_trap(left) && !is_trap(center) &&  is_trap(right) { return '^'; } // rule 4
    return '.'; // otherwise
}

fn count_safe(map: &Vec<String>) -> usize {
    map.iter()
        .map(|x| x.chars().filter(|&y| y == '.').count())
        .sum()
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut map: Vec<String> = Vec::new();
    map.push(format!("|{}|",&input));

    // Generate rows
    for _ in 1..40 {
        let mut new_row = String::from("|");
        let last_row: Vec<char> = map.iter().last().unwrap().chars().collect();

        // Generate columns
        for chs in last_row.windows(3) {
            let (left,right,center) = (chs[0], chs[1], chs[2]);
            new_row.push_str(&new_tile(left, right, center).to_string());
        }
        new_row.push_str(&String::from("|"));
        map.push(new_row);
    }

    let num_safe = count_safe(&map);
    println!("Part 1: {}", num_safe); // 2035

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut map: Vec<String> = Vec::new();
    map.push(format!("|{}|",&input));

    // Generate rows
    for _ in 1..400_000 {
        let mut new_row = String::from("|");
        let last_row: Vec<char> = map.iter().last().unwrap().chars().collect();

        // Generate columns
        for chs in last_row.windows(3) {
            let (left,right,center) = (chs[0], chs[1], chs[2]);
            new_row.push_str(&new_tile(left, right, center).to_string());
        }
        new_row.push_str(&String::from("|"));
        map.push(new_row);
    }

    let num_safe = count_safe(&map);
    println!("Part 2: {}", num_safe); // 20000577

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}