use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

#[macro_use]
extern crate text_io;

struct Disc {
    id: i64,
    positions: i64,
    initial: i64,
}
impl From<&String> for Disc {
    fn from(input: &String) -> Self {
        let id: i64;
        let positions: i64;
        let initial: i64;
        scan!(input.bytes() => "Disc #{} has {} positions; at time=0, it is at position {}.",id,positions,initial);
        Self { id, positions, initial }
    }
}
impl Disc {
    pub fn passthru(&self, time: i64) -> bool {
        (time - (self.positions - self.initial)) % self.positions == 0
    }
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let discs: Vec<Disc> = input.iter().map(Disc::from).collect();

    let mut wait = 0;
    loop {
        let res = discs.iter().filter(|x| x.passthru(wait+x.id)).count();
        if res == discs.len() { break; }
        wait += 1; 
    }

    println!("Part 1: {}", wait); // 121834

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let mut discs: Vec<Disc> = input.iter().map(Disc::from).collect();
    discs.push(Disc::from(&"Disc #7 has 11 positions; at time=0, it is at position 0.".to_string()));

    let mut wait = 0;
    loop {
        let res = discs.iter().filter(|x| x.passthru(wait+x.id)).count();
        if res == discs.len() { break; }
        wait += 1; 
    }

    println!("Part 2: {}", wait); // 3208099

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}