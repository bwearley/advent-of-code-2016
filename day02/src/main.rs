use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::cmp;

#[derive(Debug, Copy, Clone)]
struct Position {
    x: i64,
    y: i64,
}
impl Position {
    pub fn new(x: i64, y: i64) -> Self {
        Self { x, y }
    }
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // X : 1 2 3
    // Y   . . .
    // 1 : 1 2 3
    // 2 : 4 5 6
    // 3 : 7 8 9

    let keypad = vec![
        vec!["1","2","3"],
        vec!["4","5","6"],
        vec!["7","8","9"]];

    // Start on 5
    let mut x: i64 = 1;
    let mut y: i64 = 1;

    let mut part1: String = String::from("");

    for line in reader.lines() {
        for c in line?.chars() {
            match c {
                'U' => y += -1,
                'L' => x += -1,
                'R' => x +=  1,
                'D' => y +=  1,
                _ => panic!("Unknown character: {}",c),
            }
            x = cmp::min(x,2); x = cmp::max(x,0);
            y = cmp::min(y,2); y = cmp::max(y,0);
        }
        part1.push_str(keypad[y as usize][x as usize]);
    }

    // Part 1
    println!("Part 1: {}", part1); // 92435

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    //     1
    //   2 3 4
    // 5 6 7 8 9
    //   A B C
    //     D

    let keypad = vec![
        vec![" "," "," "," "," "," "," "],
        vec![" "," "," ","1"," "," "," "],
        vec![" "," ","2","3","4"," "," "],
        vec![" ","5","6","7","8","9"," "],
        vec![" "," ","A","B","C"," "," "],
        vec![" "," "," ","D"," "," "," "],
        vec![" "," "," "," "," "," "," "]];

    // Start on 5
    let mut pos: Position = Position::new(1, 3);

    let mut part2: String = String::from("");

    for line in reader.lines() {
        for c in line?.chars() {
            let pos_old = pos;
            match c {
                'U' => pos.y += -1,
                'L' => pos.x += -1,
                'R' => pos.x +=  1,
                'D' => pos.y +=  1,
                _ => panic!("Unknown character: {}",c),
            }
            if keypad[pos.y as usize][pos.x as usize] == " " {
                pos = pos_old;
            }
        }
        part2.push_str(keypad[pos.y as usize][pos.x as usize]);
    }

    // Part 2
    println!("Part 2: {}", part2); // C1A88

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}