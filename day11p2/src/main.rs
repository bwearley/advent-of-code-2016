use std::collections::VecDeque;
use std::collections::HashSet;

extern crate itertools;
use itertools::Itertools;

const NUM_ELEM: usize = 7;
const NUM_FLOOR: usize = 4;

#[derive(Debug, Clone, Copy, Hash)]
struct FaciltyState {
    elev: usize,
    rtgs: [usize; NUM_ELEM],
    chips: [usize; NUM_ELEM],
    steps: usize,
}
impl FaciltyState {
    pub fn is_valid(&self) -> bool {
        //if self.elev < 1 || self.elev > NUM_FLOOR { return false; }
        for (ch_ix,ch) in self.chips.iter().enumerate() {
            if ch == &self.rtgs[ch_ix] { continue; } // good: if on floor with own generator
            if self.rtgs.iter().filter(|g| g == &ch).count() > 0 { return false; } // bad: if any other generator on same floor as chip
        }
        true
    }
    pub fn base_repr(&self) -> FaciltyState {
        let mut combos: Vec<(usize, usize)> = Vec::new();
        for it in self.chips.iter().zip(self.rtgs.iter()) {
            let (ch,gen) = it;
            combos.push((*ch,*gen));
        }
        combos.sort();
        FaciltyState {
            elev: self.elev,
            rtgs:  [ combos[0].1, combos[1].1, combos[2].1, combos[3].1, combos[4].1, combos[5].1, combos[6].1],
            chips: [ combos[0].0, combos[1].0, combos[2].0, combos[3].0, combos[4].0, combos[5].0, combos[6].0],
            steps: 0,
        }
    }
    pub fn print_repr(&self) {
        for floor in (1..=NUM_FLOOR).rev() {
            let elev  = if self.elev  == floor { "E"  } else { "."  };
            let rtg0  = if self.rtgs[0]  == floor { "G0" } else { ". " };
            let rtg1  = if self.rtgs[1]  == floor { "G1" } else { ". " };
            let rtg2  = if self.rtgs[2]  == floor { "G2" } else { ". " };
            let rtg3  = if self.rtgs[3]  == floor { "G3" } else { ". " };
            let rtg4  = if self.rtgs[4]  == floor { "G4" } else { ". " };
            let rtg5  = if self.rtgs[5]  == floor { "G5" } else { ". " };
            let rtg6  = if self.rtgs[6]  == floor { "G6" } else { ". " };
            let chip0 = if self.chips[0] == floor { "C0" } else { ". " };
            let chip1 = if self.chips[1] == floor { "C1" } else { ". " };
            let chip2 = if self.chips[2] == floor { "C2" } else { ". " };
            let chip3 = if self.chips[3] == floor { "C3" } else { ". " };
            let chip4 = if self.chips[4] == floor { "C4" } else { ". " };

            let chip5 = if self.chips[5] == floor { "C5" } else { ". " };
            let chip6 = if self.chips[6] == floor { "C6" } else { ". " };
            println!(" F{}  {}  {}  {}  {}  {}  {}  {}  {}  {}  {}  {}   {}  {}   {}  {}", 
                floor, elev, rtg0, chip0, rtg1, chip1, rtg2, chip2, rtg3, chip3, rtg4, chip4, rtg5, chip5, rtg6, chip6);
        }
        println!("Steps: {}", self.steps);
    }
}
impl PartialEq for FaciltyState {
    fn eq(&self, other: &Self) -> bool {
        self.elev  == other.elev  &&
        self.rtgs  == other.rtgs  &&
        self.chips == other.chips
    }
}
impl Eq for FaciltyState {}

// The first floor contains a thulium generator, a thulium-compatible microchip, a plutonium generator, and a strontium generator.
// The second floor contains a plutonium-compatible microchip and a strontium-compatible microchip.
// The third floor contains a promethium generator, a promethium-compatible microchip, a ruthenium generator, and a ruthenium-compatible microchip.
// The fourth floor contains nothing relevant.
fn day11() {

    let mut q: VecDeque<FaciltyState> = VecDeque::new();
    let mut seen: HashSet<FaciltyState> = HashSet::new();
    
    //                                  dilithium -,           dilithium -,
    //                                   elerium -, \            elerium -, \
    //                                ruthenium -, \ \        ruthenium -, \ \
    //                              promethium -, \ \ \     promethium -, \ \ \
    //                              plutonium -, \ \ \ \    plutonium -, \ \ \ \
    //                             strontium -, \ \ \ \ \  strontium -, \ \ \ \ \
    //                              thulium -, \ \ \ \ \ \  thulium -, \ \ \ \ \ \
    //                                        v v v v v v v           v v v v v v v
    //                                        0 1 2 3 4 5 6           0 1 2 3 4 5 6  
    let start = FaciltyState{ elev: 1, rtgs: [1,1,1,3,3,1,1], chips: [1,2,2,3,3,1,1], steps: 0 };
    let goal  = FaciltyState{ elev: 4, rtgs: [4,4,4,4,4,4,4], chips: [4,4,4,4,4,4,4], steps: 0 };

    seen.insert(start);

    println!("Starting state: ");
    start.print_repr();

    q.push_back(start);

    let mut shortest: usize = std::usize::MAX;
    while q.len() > 0 {
        
        let node = q.pop_front().unwrap();
        //node.print_repr();

        if node == goal {
            if node.steps < shortest { shortest = node.steps }
            node.print_repr();
            break;
        }

        let possible_chips: Vec<usize> = node.chips.iter().enumerate().filter(|(_,ch)| *ch == &node.elev).map(|(i,_)| i).collect();
        let possible_rtgs: Vec<usize>  =  node.rtgs.iter().enumerate().filter(|(_,ch)| *ch == &node.elev).map(|(i,_)| i).collect();

        let new_steps = node.steps + 1;
        
        // ELEVATOR UP
        if node.elev < NUM_FLOOR {
            let new_elev = node.elev + 1;

            // Try moving 2 chips
            for comb in possible_chips.clone().iter().combinations(2) {
                let mut new_chips = node.chips;
                new_chips[*comb[0]] += 1;
                new_chips[*comb[1]] += 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: node.rtgs, chips: new_chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 2 RTGs
            for comb in possible_rtgs.clone().iter().combinations(2) {
                let mut new_rtgs = node.rtgs;
                new_rtgs[*comb[0]] += 1;
                new_rtgs[*comb[1]] += 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: node.chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 1 chip & 1 RTG
            for ch in &possible_chips {
                for rt in &possible_rtgs {
                    let mut new_chips = node.chips;
                    let mut new_rtgs = node.rtgs;
                    new_chips[*ch] += 1;
                    new_rtgs[*rt] += 1;
                    let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: new_chips, steps: new_steps };
                    let new_base = new_state.base_repr();
                    if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
                }
            }

            // Try moving 1 chip
            for ch in &possible_chips {
                let mut new_chips = node.chips;
                new_chips[*ch] += 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: node.rtgs, chips: new_chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 1 RTG
            for rt in &possible_rtgs {
                let mut new_rtgs = node.rtgs;
                new_rtgs[*rt] += 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: node.chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

        }
        
        if node.elev > 1 {
            // Try moving elevator down
            let new_elev = node.elev - 1;

            // Try moving 2 chips
            for comb in possible_chips.clone().iter().combinations(2) {
                let mut new_chips = node.chips;
                new_chips[*comb[0]] -= 1;
                new_chips[*comb[1]] -= 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: node.rtgs, chips: new_chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 2 RTGs
            for comb in possible_rtgs.clone().iter().combinations(2) {
                let mut new_rtgs = node.rtgs;
                new_rtgs[*comb[0]] -= 1;
                new_rtgs[*comb[1]] -= 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: node.chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 1 chip & 1 RTG
            for ch in &possible_chips {
                for rt in &possible_rtgs {
                    let mut new_chips = node.chips;
                    let mut new_rtgs = node.rtgs;
                    new_chips[*ch] -= 1;
                    new_rtgs[*rt] -= 1;
                    let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: new_chips, steps: new_steps };
                    let new_base = new_state.base_repr();
                    if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
                }
            }

            // Try moving 1 chip
            for ch in &possible_chips {
                let mut new_chips = node.chips;
                new_chips[*ch] -= 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: node.rtgs, chips: new_chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

            // Try moving 1 RTG
            for rt in &possible_rtgs {
                let mut new_rtgs = node.rtgs;
                new_rtgs[*rt] -= 1;
                let new_state = FaciltyState{ elev: new_elev, rtgs: new_rtgs, chips: node.chips, steps: new_steps };
                let new_base = new_state.base_repr();
                if new_state.is_valid() && !seen.contains(&new_base) { q.push_back(new_state); seen.insert(new_base); }
            }

        }

    }
    println!("Part 2: {}", shortest); // 55
}

fn main() {
    day11();
}