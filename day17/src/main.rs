use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;

use crypto::md5::Md5;
use crypto::digest::Digest;

const U: usize = 0;
const D: usize = 1;
const L: usize = 2;
const R: usize = 3;

struct MapPoint {
    x: i64,
    y: i64,
    d: i64,
    path: String,
}

fn hash_path(input: &str, path: &str) -> String {
    let mut digest = Md5::new();
    let hash = format!("{}{}", &input, &path);
    digest.reset();
    digest.input_str(&hash);
    digest.result_str()
}

fn is_unlocked(ch: char) -> bool {
    match ch {
        'b'..='f' => true,
        _ => false,
    }
}

fn doors_status(input: &str, path: &str) -> Vec<bool> {
    hash_path(&input, &path)
        .chars()
        .take(4)
        .map(|c| is_unlocked(c))
        .collect()
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut q: VecDeque<MapPoint> = VecDeque::new();

    let target_node = (3, 3); // 4x4 grid, 0-indexed

    q.push_back(MapPoint{x: 0, y: 0, d: 0, path: String::new()});

    let mut shortest: i64 = std::i64::MAX;
    let mut shortest_path = "".to_string();
    while q.len() > 0 {
        let node = q.pop_front().unwrap();

        if (node.x, node.y) == target_node && node.d < shortest {
            shortest = node.d;
            shortest_path = node.path.clone();
        }

        if (node.x, node.y) == target_node { continue; }

        let doors = doors_status(&input, &node.path);
        let new_dist = &node.d + 1;
        if new_dist > shortest { continue; }
        if doors[U] && node.y-1 >= 0 { q.push_back(MapPoint{x: node.x,   y: node.y-1, d: new_dist, path: format!("{}{}",node.path,"U") }) }
        if doors[D] && node.y+1 <= 3 { q.push_back(MapPoint{x: node.x,   y: node.y+1, d: new_dist, path: format!("{}{}",node.path,"D") }) }
        if doors[L] && node.x-1 >= 0 { q.push_back(MapPoint{x: node.x-1, y: node.y,   d: new_dist, path: format!("{}{}",node.path,"L") }) }
        if doors[R] && node.x+1 <= 3 { q.push_back(MapPoint{x: node.x+1, y: node.y,   d: new_dist, path: format!("{}{}",node.path,"R") }) }
    }
    println!("Part 1: {}", shortest_path); // RDURRDDLRD

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut q: VecDeque<MapPoint> = VecDeque::new();

    let target_node = (3, 3); // 4x4 grid, 0-indexed

    q.push_back(MapPoint{x: 0, y: 0, d: 0, path: "".to_string()});

    let mut longest: i64 = 0;
    while q.len() > 0 {
        let node = q.pop_front().unwrap();

        if (node.x, node.y) == target_node && node.d > longest { longest = node.d; }

        if (node.x, node.y) == target_node { continue; }

        let doors = doors_status(&input, &node.path);
        let new_dist = &node.d + 1;
        if doors[U] && node.y-1 >= 0 { q.push_back(MapPoint{x: node.x,   y: node.y-1, d: new_dist, path: format!("{}{}",node.path,"U") }) }
        if doors[D] && node.y+1 <= 3 { q.push_back(MapPoint{x: node.x,   y: node.y+1, d: new_dist, path: format!("{}{}",node.path,"D") }) }
        if doors[L] && node.x-1 >= 0 { q.push_back(MapPoint{x: node.x-1, y: node.y,   d: new_dist, path: format!("{}{}",node.path,"L") }) }
        if doors[R] && node.x+1 <= 3 { q.push_back(MapPoint{x: node.x+1, y: node.y,   d: new_dist, path: format!("{}{}",node.path,"R") }) }
    }
    println!("Part 2: {}", longest); // 526

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}