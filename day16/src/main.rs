use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn swap_01(c: &char) -> char {
    match c {
        '0' => '1',
        '1' => '0',
        other => panic!("Unexpected character: {}", other),
    }
}

fn expand(a: &str) -> String {
    let b: String = a
        .chars()
        .rev()
        .map(|x| swap_01(&x))
        .collect();
    a.to_string() + "0" + &b
}

fn checksum(input: &str) -> String {
    let mut chksm: String = String::with_capacity(input.len() / 2);
    for ch in input.chars().collect::<Vec<_>>().chunks(2) {
        if ch[0] == ch[1] {
            chksm.push_str("1");
        } else {
            chksm.push_str("0");
        }
    }
    if chksm.len() % 2 == 0 { chksm = checksum(&chksm); }
    chksm
}

fn solve(input: &str, min_len: usize) -> String {
    let mut res = String::from(input);
    while res.len() < min_len {
        res = expand(&res);
    }
    checksum(&res[0..min_len])
}

fn day16(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Solutions
    println!("Part 1: {}", solve(&input, 272));      // Part 1: 10010110010011110
    println!("Part 2: {}", solve(&input, 35651584)); // Part 2: 01101011101100011

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day16(&filename).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn expand_1() { assert!(expand("1") == "100"); }
    #[test]
    fn expand_0() { assert!(expand("0") == "001"); }
    #[test]
    fn expand_11111() { assert!(expand("11111") == "11111000000"); }
    #[test]
    fn expand_111100001010() { assert!(expand("111100001010") == "1111000010100101011110000"); }
    #[test]
    fn expand_110010110100() { assert!(checksum("110010110100") == "100"); }
    #[test]
    fn solve_10000_20() { assert!(solve("10000", 20) == "01100"); }
}