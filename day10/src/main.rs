use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;

#[macro_use]
extern crate text_io;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    day10(&filename).unwrap();
}

#[derive(Clone)]
struct Bot {
    chips: Vec<usize>,
}
impl Bot {
    pub fn new() -> Bot {
        Bot {
            chips: Vec::new(),
        }
    }
}

struct Instruction {
    instr_type: InstructionType,
    target: usize,
    value: usize,
    high: usize,
    low: usize,
    high_target: TargetType,
    low_target: TargetType,
}
impl Instruction {
    pub fn from_string(input: &str) -> Instruction {
        let value: usize;
        let bot: usize;
        let high: usize;
        let low: usize;
        let high_type: String;
        let low_type: String;
        match &input.split_whitespace().next() {
            Some("value") => {
                scan!(input.bytes() => "value {} goes to bot {}", value, bot);
                return Instruction {
                    instr_type: InstructionType::Take,
                    target: bot,
                    value: value,
                    high: 0,
                    low: 0,
                    high_target: TargetType::None,
                    low_target: TargetType::None,
                };
            },
            Some("bot") => {
                scan!(input.bytes() => "bot {} gives low to {} {} and high to {} {}", bot, low_type, low, high_type, high);
                return Instruction {
                    instr_type: InstructionType::Give,
                    target: bot,
                    value: 0,
                    high: high,
                    low: low,
                    high_target: if high_type == "bot" { TargetType::Bot } else { TargetType::Output },
                    low_target:  if low_type  == "bot" { TargetType::Bot } else { TargetType::Output },
                };
            },
            Some(other) => panic!("Received unknown value ({}) reading input: {}", other, input),
            None => panic!("Unknown error reading input: {}", input),
        }
    }
}

enum InstructionType {
    Give,
    Take,
}

#[derive(PartialEq)]
enum TargetType {
    Bot,
    Output,
    None,
}

fn day10(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let instr: Vec<Instruction> = input.iter().map(|x| Instruction::from_string(x)).collect();

    // Make bots
    // println!("Max bot: {}", instr.iter().max_by_key(|x| x.target).unwrap().target); // 209
    let mut bots: Vec<Bot> = vec![Bot::new(); 250];

    // Make outputs
    let mut outputs: Vec<Vec<usize>> = vec![Vec::new(); 250];

    // Instruction queue
    let mut q: VecDeque<Instruction> = VecDeque::from(instr);

    // Process queue of instructions
    while q.len() > 0 {
        let node = q.pop_front().unwrap();
        match node.instr_type {
            InstructionType::Give => {
                
                // Part 1 Answer
                if bots[node.target].chips.len() == 2 {
                    if bots[node.target].chips[0] == 17 && bots[node.target].chips[1] == 61 {
                        println!("Part 1: {}", node.target); // 73
                    }
                }
                
                if bots[node.target].chips.len() == 2 {
                    let low = bots[node.target].chips[0];
                    let high = bots[node.target].chips[1];

                    match node.low_target {
                        TargetType::Bot => {
                            if bots[node.low].chips.len() < 2 {
                                bots[node.low].chips.push(low);
                                bots[node.low].chips.sort();
                            }
                        },
                        TargetType::Output => {
                            outputs[node.low].push(low);
                        },
                        TargetType::None => panic!("Invalid target type for low of bot {}", node.target),
                    }

                    match node.high_target {
                        TargetType::Bot => {
                            if bots[node.high].chips.len() < 2 {
                                bots[node.high].chips.push(high);
                                bots[node.high].chips.sort(); 
                            }
                        },
                        TargetType::Output => {
                            outputs[node.high].push(high);
                        },
                        TargetType::None => panic!("Invalid target type for high of bot {}", node.target),
                    }

                    // Remove chips
                    bots[node.target].chips.remove(1);
                    bots[node.target].chips.remove(0);

                } else {
                    q.push_back(node); // Defer
                }
            },
            
            InstructionType::Take => {
                if bots[node.target].chips.len() < 2 {
                    bots[node.target].chips.push(node.value);
                    bots[node.target].chips.sort();
                } else {
                    q.push_back(node); // Defer
                }
            },
        }
    }

    println!("Part 2: {}", outputs[0][0] * outputs[1][0] * outputs[2][0]); // 3965

    Ok(())
}