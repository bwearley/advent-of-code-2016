use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use permutator::{Permutation};

extern crate permutator;

fn scrambler(rules: &Vec<String>, input: &str) -> String {
    let mut scrambled: Vec<char> = input.chars().collect();

    for cmd in rules {
        let cmd_parts: Vec<_> = cmd.split_whitespace().collect();

        match cmd_parts[0] {
            "rotate" => {
                match cmd_parts[1] {
                    "right" => scrambled.rotate_right(cmd_parts[2].parse::<usize>().unwrap()),
                    "left" =>   scrambled.rotate_left(cmd_parts[2].parse::<usize>().unwrap()),
                    "based" => {
                        let ix = scrambled.iter().enumerate().find(|x| x.1.to_string() == cmd_parts[6]).unwrap().0;
                        let mut rotations = if ix >= 4 { 1 + ix + 1 } else { 1 + ix };
                        rotations %= input.len();
                        scrambled.rotate_right(rotations);
                    },
                    other => panic!("Unknown rotation type: {}", other),
                }
            },
            "swap" => {
                match cmd_parts[1] {
                    "position" => {
                        //swap position 1 with position 3
                        let pos1 = cmd_parts[2].parse::<usize>().unwrap();
                        let pos2 = cmd_parts[5].parse::<usize>().unwrap();
                        scrambled.swap(pos1,pos2);
                    },
                    "letter" => {
                        //swap letter b with letter e
                        let pos1 = scrambled.iter().enumerate().find(|x| x.1.to_string() == cmd_parts[2]).unwrap().0;
                        let pos2 = scrambled.iter().enumerate().find(|x| x.1.to_string() == cmd_parts[5]).unwrap().0;
                        scrambled.swap(pos1,pos2);
                    },
                    other => panic!("Unknown swap type: {}", other),
                }
            },
            "reverse" => {
                // reverse positions 1 through 4
                let pos1 = cmd_parts[2].parse::<usize>().unwrap();
                let pos2 = cmd_parts[4].parse::<usize>().unwrap();
                scrambled[pos1..=pos2].reverse();
            },
            "move" => {
                //move position 6 to position 5
                let pos1 = cmd_parts[2].parse::<usize>().unwrap();
                let pos2 = cmd_parts[5].parse::<usize>().unwrap();
                let removed = scrambled[pos1];
                scrambled.remove(pos1);
                scrambled.push('_');
                scrambled[pos2..].rotate_right(1);
                scrambled[pos2] = removed;
            },
            other => panic!("Unknown command: {}", other),
        }
    }
    scrambled.iter().map(|x| x.to_string()).collect::<Vec<String>>().concat()
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    println!("Part 1: {}", scrambler(&input, "abcdefgh")); // dbfgaehc

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let goal = "fbgdceah";

    let mut letters: Vec<char> = String::from("abcdefgh").chars().collect();

    for permute in letters.permutation() {
        let pw = permute.iter().map(|x| x.to_string()).collect::<Vec<String>>().concat();
        if scrambler(&input, &pw) == goal {
            println!("Part 2: {}", pw); // aghfcdeb
            break;
        }
    }

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}