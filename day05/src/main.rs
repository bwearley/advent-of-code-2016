use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

//extern crate md5;
use crypto::md5::Md5;
use crypto::digest::Digest;

fn day05(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    
    // Get input from file
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: String = input.concat();

    // Generate password
    let mut part1_pass = String::new();
    let mut part2_pass = vec![String::from("_"); 8];
    let mut part1_len: i64 = 0;
    let mut part2_len: i64 = 0;
    let mut index: i64 = 0;
    let mut digest = Md5::new();
    while part1_len < 8 || part2_len < 8 {
        let hash = format!("{}{}",input, index);
        digest.reset();
        digest.input_str(&hash);
        let res = digest.result_str();
        if res.starts_with("00000") {
            // Part 1
            if part1_len < 8 {
                part1_pass.push_str(&res[5..6]);
                part1_len += 1;
                if part1_len == 8 { println!("Part 1: {}", part1_pass); } // Part 1: 4543c154
            }
            // Part 2
            if part2_len < 8 {
                if let Ok(pos) = res.get(5..6).unwrap().parse::<usize>() {
                    match pos {
                        0..=7 => if part2_pass[pos] == "_" {
                            part2_pass[pos] = res[6..7].to_string();
                            part2_len += 1;
                        },
                        _ => (),
                    }
                }
            }
        }
        index += 1;
    }

    // Outputs
    //println!("Part 1: {}", part1_pass); // Part 1: 4543c154
    println!("Part 2: {}", part2_pass.concat()); // Part 2: 1050cbbd

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day05(&filename).unwrap();
}