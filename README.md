# Advent of Code 2016

My Advent of Code 2016 submissions performed in Rust.

To build and run each solution, 

`cargo run input.txt --release`