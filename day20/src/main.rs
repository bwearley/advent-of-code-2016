use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::cmp;

#[derive(Debug, Copy, Clone)]
struct IPRange {
    min: i64,
    max: i64,
}
impl IPRange {
    pub fn from_string(input: &str) -> Self {
        let range = input
            .split("-")
            .map(|x| x.parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        Self {
            min: range[0],
            max: range[1],
        }
    }
}

fn day20(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Sort blacklist ranges by minimums
    let mut ranges_in: Vec<IPRange> = input
        .iter()
        .map(|x| IPRange::from_string(x))
        .collect();
    ranges_in.sort_by(|a,b| a.min.cmp(&b.min));

    // Merge blacklist ranges
    let mut ranges: Vec<IPRange> = Vec::new();
    ranges.push(ranges_in[0]);
    for r in ranges_in {
        let last_ix = &ranges.len()-1;
        let last = &ranges[last_ix];
        if r.min <= last.max + 1 {
            ranges[last_ix].max = cmp::max(ranges[last_ix].max, r.max);
        } else {
            ranges.push(r);
        }
    }

    println!("Part 1: {}", &ranges[0].max+1); // 14975795

    // Calculate valid IPs not in range
    let ip_pool_size: i64 = 4_294_967_295;
    let num_blacklisted: i64 = ranges.iter().map(|x| x.max - x.min + 1).sum();

    println!("Part 2: {}", ip_pool_size-num_blacklisted+1); // 101

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day20(&filename).unwrap();
}