use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate regex;
use regex::Regex;

struct IPAddress {
    address: Vec<String>,
    hypernet: Vec<String>,
}
impl IPAddress {
    pub fn from_string(input: &str) -> Self {
        let re = Regex::new(r"\[(.*?)\]").unwrap();
        let hypernet = re.captures_iter(input).map(|x| x[1].to_string()).collect();

        let re = Regex::new(r"(.*?)\[(.*?)\]").unwrap();
        let s = input.to_owned()+"[.]"; // Horrible hack for bad regex
        let address = re.captures_iter(&s).map(|x| x[1].to_string()).collect();

        Self { address, hypernet }
    }
    pub fn supports_tls(&self) -> bool {
        let address_supports = self.address.iter().filter(|x| contains_abba(x)).count() > 0;
        let hypernet_disables = self.hypernet.iter().filter(|x| contains_abba(x)).count() > 0;
        address_supports && !hypernet_disables 
    }
    pub fn supports_ssl(&self) -> bool {
        //let abas: Vec<ABA> = self.address.iter().flat_map(|x| get_aba(x)).collect();
        //let babs: Vec<ABA> = self.hypernet.iter().flat_map(|x| get_aba(x)).collect();
        let abas: Vec<ABA> = self.address.iter().map(|x| get_aba(x)).flatten().collect();
        let babs: Vec<ABA> = self.hypernet.iter().map(|x| get_aba(x)).flatten().collect();
        for aba in &abas {
            for bab in &babs {
                if aba.matches_bab(bab) { return true; }
            }
        }
        return false;
    }
}

#[derive(Debug, Copy, Clone)]
struct ABA {
    a: char,
    b: char,
}
impl ABA {
    pub fn from_chars(a: char, b: char) -> Self {
        Self { a, b }
    }
    pub fn matches_bab(&self, other: &ABA) -> bool {
        self.a == other.b && self.b == other.a
    }
}

fn contains_abba(input: &str) -> bool {
    let chars: Vec<char> = input.chars().collect();
    for ch in chars.windows(4) {
        let (a,b,c,d) = (ch[0], ch[1], ch[2], ch[3]);
        if a == d && b == c && a != b { return true; }
    }
    false
}

fn get_aba(input: &str) -> Vec<ABA> {
    let mut abas: Vec<ABA> = Vec::new();
    let chars: Vec<char> = input.chars().collect();
    for ch in chars.windows(3) {
        let (a1,b,a2) = (ch[0], ch[1], ch[2]);
        if a1 == a2 && a1 != b { abas.push(ABA::from_chars(a1,b)); }
    }
    abas
}

fn day07(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let addrs: Vec<IPAddress> = reader.lines().map(|l| IPAddress::from_string(&l.unwrap())).collect();

    // Outputs
    let part1 = addrs.iter().filter(|x| x.supports_tls()).count();
    println!("Part 1: {}", part1); // 110
    let part2 = addrs.iter().filter(|x| x.supports_ssl()).count();
    println!("Part 2: {}", part2); // 242

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day07(&filename).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tls() {
        assert!( IPAddress::from_string("abba[mnop]qrst").supports_tls());
        assert!(!IPAddress::from_string("abcd[bddb]xyyx").supports_tls());
        assert!( IPAddress::from_string("abcd[bdab]xyyx").supports_tls());
        assert!(!IPAddress::from_string("aaaa[qwer]tyui").supports_tls());
        assert!( IPAddress::from_string("ioxxoj[asdfgh]zxcvbn").supports_tls());
        assert!( IPAddress::from_string("jjioxxo[asdfgh]zxcvbn").supports_tls());
        assert!(!IPAddress::from_string("jjioxxo[abbaasdfgh]zxcvbn").supports_tls());
        assert!( IPAddress::from_string("zxcvbnabba[asdfgh]jjioxfxo").supports_tls());
        assert!( IPAddress::from_string("jjioxfxo[asdfgh]zxcvbnabba").supports_tls());
    }

}