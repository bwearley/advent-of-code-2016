use std::env;
use std::collections::HashSet;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate num_complex;
use num_complex::Complex;
//use num::complex::Complex;

struct Direction {
    turn: Complex<i64>,
    mag: i64,
}
impl Direction {
    pub fn new(inp: &str) -> Direction{
        let turn = &inp[0..1];
        let left = Complex::new(0, 1);
        let right = Complex::new(0, -1);
        Direction {
            turn: if turn == "L" { left } else { right },
            mag: inp[1..].parse::<i64>().unwrap(),
        }
    }
}

fn day01(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let directions: Vec<Direction> = input
        .concat()
        .split(", ")
        .map(|x| Direction::new(x))
        .collect();

    let mut pos = Complex::new(0, 0);
    let mut dir = Complex::new(0, 1);
    let mut seen = HashSet::new();
    let mut part2ans: i64 = 0;

    for direction in directions {
        dir *= direction.turn;
        for _ in 0..direction.mag {
            pos += dir;
            if seen.contains(&pos) && part2ans == 0 {
                part2ans = pos.re.abs() + pos.im.abs();
            } else {
                seen.insert(pos);
            }
        }
    }

    let part1ans = pos.re.abs() + pos.im.abs();

    println!("Part 1: {}", part1ans); // 230
    println!("Part 2: {}", part2ans); // 154

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day01(&filename).unwrap();
}